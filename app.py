""" Ticker information for companies """
import asyncio
import time
import random

from flask import Flask, jsonify, abort, request

APP = Flask(__name__)


@APP.route('/health')
def health_check():
    """ API for health checks """
    return jsonify({'healthy': True})

# Three companies with various start prices,
# The ticker will be some variance around this
COMPANIES = dict(a=330, b=86) #, c=5.30)

def calculate_next_price(prev_price):
    """
    these will be highly wild volatility
    +/- 5%
    """
    offset = random.random() / 10 - 0.05
    return prev_price + (prev_price * offset)


@APP.route('/<company>/ticker', methods=['GET'])
def ticker(company):
    """ API for a company ticker """
    print(request.headers)
    base_price = COMPANIES.get(company)
    if not base_price:
        abort(404)
    price = calculate_next_price(base_price)
    return jsonify(dict(company=company, price=price))


async def handle_echo(reader, writer):
    """ async handler for connections """
    try:
        data = await reader.read(100)
        message = data.decode().rstrip()
        addr = writer.get_extra_info('peername')

        print(f"Received {message} from {addr!r}")

        # Lookup the company sent at the beginning of the connection.
        # This will establish the stream of ticker data for that company
        company = COMPANIES.get(message)
        if not company:
            writer.write("Company not found, closing connection.\n".encode())
            await writer.drain()
            return

        # We have a valid company, go ahead and push
        # ticker data to them
        while True:
            tick = calculate_next_price(company)
            # using a string rather than packed double here for easy pretty printing
            writer.write(f"{tick}\n".encode())
            await writer.drain()
            time.sleep(0.5)
    finally:
        writer.close()


async def main():
    """ TCP main """
    # Again, note that we bind to localhost, forcing all traffic
    # to go through the proxy
    print("Starting server..")
    server = await asyncio.start_server(handle_echo, '127.0.0.1', 8888)

    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')

    async with server:
        await server.serve_forever()


if __name__ == "__main__":
    asyncio.run(main())
