""" Testing for the ticker service """
import os
import pytest
import requests
from ticker.app import calculate_next_price

TICKER_HOSTNAME = os.environ.get('TICKER_HOSTNAME', 'ticker')


@pytest.mark.unit
def test_next_price_is_within_5_percent():
    """ Check that the next price is within 5 percent of the original price """
    price = calculate_next_price(100)
    assert price > 95
    assert price < 105
    price = calculate_next_price(100)
    assert price > 95
    assert price < 105


@pytest.mark.integration
def test_health_check():
    """ Checks the health of the service """
    reply = requests.get(f"http://{TICKER_HOSTNAME}/health")
    assert reply.status_code == 200
    assert reply.json() == dict(healthy=True)


@pytest.mark.integration
def test_ticker_api():
    """ Checks the ticker services gives valid data for an existing service """
    reply = requests.get(f"http://{TICKER_HOSTNAME}/a/ticker")
    assert reply.status_code == 200
    assert reply.json().get('company') == 'a'
