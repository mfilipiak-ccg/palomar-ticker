# Palomar Ticker

The ticker component provides current pricing information for a given company. That pricing information can be retrieved in two ways:

* A REST API that can be queried by anybody
* A TCP socket that will retrieve a stream of ticker information every half second

Because this service is public, it has a subdomain specified on the Gateway.  This makes it routable through `ticker.domain.com`. See [Gateway Definition](chart/templates/ticker-gateways.yaml).

## REST API

### GET /{company}/ticker

This API is public and open to all users.

```

Arguments:
    company:
        A valid company identifier

Returns
    200:
        company: The company requested
        price: The current price of the company

    404:
        The company was not found
```

## Ticker TCP stream

Connect to `ticker.domain.com:8888` and send the company name to initiate a stream of ticker information.

## Pipeline

See [Gitlab CI configuration](.gitlab-ci.yml)

Locally, this project manages it's testing pipeline via docker. Since it has no external dependencies (from an application standpoint), all tests are achievable in this environment.

It also supports running within the context of an upstream integration environment. That upstream environment is kubernetes based, and is more opinionated. See upstream https://gitlab.com/mfilipiak-ccg/palomar project for details, and use this as reference.

The strategy in this project is to embedd the integrated environment in the same file. For simple projects, this is doable, but can cause noise in larger projects. It is also reasonable for pipeline's that are also kubernetes based. Otherwise, it is recommended to use a separate integration pipeline file. Examples of this can be found in other palomar projects.