# TODO the alpine image gives some DNS issues
FROM python:3-buster

RUN pip install flask requests Requests-OpenTracing pytest

COPY app.py /app/app.py
COPY tests /tests

ENV FLASK_APP=app.py
WORKDIR /app

RUN ln -s /app /usr/local/lib/python3.8/site-packages/ticker

CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]
